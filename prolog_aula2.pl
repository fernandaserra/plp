% Árvore genealógica
% 
% 
homem(orville).
homem(abraham).
homem(clancy).
homem(herb).
homem(homer).
homem(bart).

mulher(yuma).
mulher(mona).
mulher(jackie).
mulher(patty).
mulher(selma).
mulher(marge).
mulher(lisa).
mulher(maggie).
mulher(ling).

progenitor(orville,abraham).
progenitor(yuma,abraham).
progenitor(abraham,herb).
progenitor(abraham,homer).
progenitor(mona,herb).
progenitor(mona,homer).
progenitor(clancy,marge).
progenitor(clancy,patty).
progenitor(clancy,selma).
progenitor(jackie,marge).
progenitor(jackie,patty).
progenitor(jackie,selma).
progenitor(selma,ling).
progenitor(homer,bart).
progenitor(homer,lisa).
progenitor(homer,maggie).
progenitor(marge,bart).
progenitor(marge,lisa).
progenitor(marge,maggie).

mae(X,Y) :- progenitor(X,Y) , mulher(X).
pai(X,Y) :- progenitor(X,Y) , homem(X).
avop(X,Y) :- progenitor(X,Z), progenitor(Z,Y) , mulher(X).
avom(X,Y) :- progenitor(X,Z), progenitor(Z,Y) , homem(X).
irmao(X,Y) :- progenitor(Z,X), progenitor(Z,Y), homem(X).
irma(X,Y) :- progenitor(Z,X), progenitor(Z,Y), mulher(X).
tio(X,Y) :- progenitor(Z,Y), irmao(X,Z).
tia(X,Y) :- progenitor(Z,Y), irma(X,Z).
primo(X,Y) :- progenitor(Z,X) , progenitor(W,Y), irmao(Z,W).
prima(X,Y) :- progenitor(Z,X) , progenitor(W,Y), irmao(Z,W).


% 5 = 2 + 3. false pois 5 não é da forma 2 + 3.
% X + Y = 2 + 3. true se x unificar com 2 e y unificar com 3.
% 5 is 2 + 3. true pois 5 tem o mesmo valor que 2 + 3.
% homem(x) = homem(bart). true se x unificar com bart.
% (1,2,3) = (X, Y, Z). true se x unifica com 1 [...]


fatorial(0,1). %declaração do caso base.
fatorial(N, F) :- Nmenos1 is N - 1,
    			  fatorial(Nmenos1, FNmenos1),
    			  F is N * FNmenos1.


tamanho([], 0).
tamanho(H|T, N) :- tamanho(T, tamanhoT),
    			   N is 1 * tamanhoT.


% assert, retract, fail, cut
% 

