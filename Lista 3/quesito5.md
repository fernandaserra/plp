### Quesito 5
* B. legibilidade, escrita e confiabilidade

	> Em quesito de legibilidade, pode-se dizer python e prolog possuem bons níveis de legibilidade. O prolog possui poucos operadores e estruturas logo facilita seu entendimento porém seu "vocabulário" é limitado enquanto python possui suporte a  blocos de códigos mais organizados, dividos por classes e suas subclasses.

	> A escrita em prolog é mais simplificada do que a escrita em python OO, dado ao fato de que existem poucas estruturas e operadores. As estruturas do prolog são compostas de fatos e regras definidos através da junção de átomos e operadores lógicos (e,ou,se,[...]), dessa forma, conclui-se que a sintaxe limitada só nos permite escrever um átomo de uma forma padrão mas ao combinar-se vários operadores e átomos obtém-se o necessário para executar um programa de forma simples e eficiente, logo, prolog possui uma escrita superior.

	> No quesito de confiabilidade, python OO não se tem um alto grau de confiabilidade. Como a confiabilidade é afetada pela verificação de tipo, apelidos e tratamento de exceções, tem-se que o prolog pode ser considerada uma linguagem bem confiável já que não é necessário checagem de tipo e possui um bom tratamento de exceções, só falha por não ter a funcionalidade de aliasing.