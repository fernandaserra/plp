relacionamento(chiara, pietro).
relacionamento(chiara, alfredo).
relacionamento(elena, alfredo).
relacionamento(elena, henrico).
relacionamento(maria, henrico).
relacionamento(maria, adriano).
relacionamento(carol, adriano).
relacionamento(carol, pietro).

pobre(pietro).
pobre(eliza).
pobre(alfredo).
pobre(maria).

rico(carol).
rico(elena).
rico(henrico).
rico(adriano).

localizacao(alfredo, segunda, contagem).
localizacao(alfredo, terca, contagem).
localizacao(alfredo, quarta, bh).
localizacao(alfredo, quinta, contagem).
localizacao(alfredo, sexta, pensao).

localizacao(henrico, segunda, pensao).
localizacao(henrico, terca, bh).
localizacao(henrico, quarta, pensao).
localizacao(henrico, quinta, pensao).
localizacao(henrico, sexta, pensao).

localizacao(eliza, segunda, pensao).
localizacao(eliza, terca, bh).
localizacao(eliza, quarta, bh).
localizacao(eliza, quinta, contagem).
localizacao(eliza, sexta, pensao).

localizacao(adriano, segunda, pensao).
localizacao(adriano, terca, pensao).
localizacao(adriano, quarta, contagem).
localizacao(adriano, quinta, pensao).
localizacao(adriano, sexta, pensao).

localizacao(elena, segunda, pensao).
localizacao(elena, terca, bh).
localizacao(elena, quarta, bh).
localizacao(elena, quinta, pensao).
localizacao(elena, sexta, pensao).

localizacao(pietro, segunda, contagem).
localizacao(pietro, terca, contagem).
localizacao(pietro, quarta, bh).
localizacao(pietro, quinta, contagem).
localizacao(pietro, sexta, pensao).

localizacao(maria, segunda, pensao).
localizacao(maria, terca, contagem).
localizacao(maria, quarta, contagem).
localizacao(maria, quinta, contagem).
localizacao(maria, sexta, pensao).

localizacao(carol, segunda, bh).
localizacao(carol, terca, bh).
localizacao(carol, quarta, bh).
localizacao(carol, quinta, pensao).
localizacao(carol, sexta, pensao).

insano(maria).
insano(adriano).

%x tem ciumes de chiara
ciumes(Y,chiara) :-	relacionamento(chiara, X), relacionamento(Y,X).
   

motivo(X,dinheiro) :- pobre(X).
motivo(X, ciumes) :- ciumes(X, chiara).
motivo(X, insanidade) :- insano(X).

roubouChave(X) :-
    			localizacao(X, segunda, contagem);
    			localizacao(X, terca, bh).

roubouPernaPietro(X) :-
    			     localizacao(X, quinta, bh);
    				 localizacao(X, quarta, contagem).
roubouMartelo(X) :-
    			 localizacao(X, quarta, pensao);
    			 localizacao(X, quinta, pensao).
% x assassinou y
assassino(X,Y) :- 
    			(localizacao(X,quinta,pensao);localizacao(X, sexta,pensao)),
    			(roubouPernaPietro(X); roubouMartelo(X)), 
    			roubouChave(X),
    			motivo(X,ciumes);motivo(X,dinheiro);motivo(X,insanidade).

% assassino(X, chiara). X = elena
% motivo(elena,X). X = ciumes
% relacionamento(elena,X), relacionamento(chiara,X). X = alfredo