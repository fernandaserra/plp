/
    Um jogo de aventura ridiculamente simples.
    
                 *-----------* 
                 | calabouco | -> dragao mata o hero se ele esta sem chave
                 | dragao    |
                 |           |
    *------------*-----------*   
    |  jardim  | hall      |
    |  chave   | espada    # saída requer chave
    |          | heroi     |
    *------------*-----------*
    
    objetivo : sair vivo do castelo

/


:- dynamic(localizacao/2).

localizacao(heroi, hall).
localizacao(objeto(espada), hall).
localizacao(objeto(quadro), hall).
localizacao(objeto(chave), jardim).
localizacao(dragao, calabouco).

porta(hall, leste, saida, fechada).
porta(hall, oeste, jardim, aberta).
porta(hall, norte, calabouco, aberta).
porta(calabouco, sul, hall, aberta).
porta(jardim, leste, hall, aberta).

# mostra a lista em forma item1, item2, ..., itemn.
# nada, (lista vazia)
# item, (lista com 1 elemento)
# item 1, item2, ..., itemn, (lista com n elementos)
mostreLista() :- write('Nada.'), nl.
mostreLista([E]) :- write(E), write('.'), nl.
mostreLista([E1, E2]) :- write(E1), write(' e '), write(E2), write('.'), nl.
mostreLista([E1, E2|RL]) :- write(E1), write(', '), write(E2|RL).

va(DIR) :-
    localizacao(heroi, LocAtual),
    porta(LocAtual, DIR, NovaLoc, aberta),
    retract(localizacao(heroi, LocAtual)),
    assert(localizacao(heroi, NovaLoc)),
    write('Agora você está em '), write(NovaLoc), nl.
va(DIR) :-
    localizacao(heroi, LocAtual),
    porta(LocAtual, DIR, _, _),
    write('A porta ao' ), write(DIR), write(' está fechada.'), nl.
va(DIR) :-
    write('Não há porta ao' ), write(DIR), nl.


descreva(Loc) :-
    findall(Dir, EstadoPorta, porta(Loc, dir, _, EstadoPorta). listaPortas),
    write('Portas :'), mostreLista(listaPortas), nl,
    findall(O, localizacao(objeto(O), hall, LObjs)),
    write('Objetos :'), mostreLista(LObjs), nl.


iniciar :-
    localizacao(heroi, LocAtual),
    write('Você está no '), write(LocAtual), write('.', nl).



